import React from 'react'
import {
    Box,
    Heading,
    Markdown,
    Stack,
    TextInput,
    Text,
    Grid,
    Button,
    Accordion,
    AccordionPanel,
    Select,
    Tab, Tabs, TextArea
} from "grommet";
import SyntaxHighlighter from 'react-syntax-highlighter';
import {Edit} from "grommet-icons";


const AddTest = () => {
    const [value, setValue] = React.useState('');

    return (
        <div className="container">
            <Heading margin="none" level={3}>Создание теста</Heading>
            <Tabs>
                <Tab title="Обычный метод">
                    <Box direction="row"

                         width={"large"}
                         pad="small"
                         justify="between"
                    >


                        <Text>Наименование теста</Text>


                        <TextInput
                            className="form-input"
                            placeholder="Наименование теста Ex: #Test_Quark-33"
                            value={templateter.name}

                            onChange={event => templateter.name = event.target.value}
                        />


                    </Box>
                    <Box direction="row"

                         width={"large"}
                         justify="between"
                         pad="small"
                    >


                        <Text>Путь до сервера</Text>


                        <TextInput
                            className="form-input"
                            placeholder="Корневый путь сервер Ex:http://localhost"
                            value={value}
                            width={"150px"}
                            onChange={event => templateter["server-host"] = event.target.value}
                        />


                    </Box>
                    <Heading margin="none" level={3}>Конфигурирование алгоритмов</Heading>
                    <Accordion>
                        <AccordionPanel label="Пути тестирования">
                            <Box direction="column"
                                 width={"large"}
                                 pad="small"
                            >


                                <Button style={{margin: "10px"}}
                                        icon={<Edit/>}
                                        label="Добавить еще путь"
                                        onClick={() => {
                                        }}
                                />


                            </Box>
                        </AccordionPanel>
                        <AccordionPanel label="Алгоритм тестирования">
                            <Box direction="column"
                                 width={"large"}
                                 pad="small"
                            >
                                <Select
                                    margin={"10px"}
                                    options={['small', 'medium', 'large']}
                                    value={value}
                                    onChange={({option}) => setValue(option)}
                                />
                                <Text> Алгоритм тестирования</Text>
                                <TextInput
                                    className="form-input"
                                    placeholder="URL для тестирования"
                                    value={value}
                                    width={"150px"}
                                    onChange={event => setValue(event.target.value)}

                                />

                                <Button style={{margin: "10px"}}
                                        icon={<Edit/>}
                                        label="Добавить еще путь"
                                        onClick={() => {
                                        }}
                                />


                            </Box>
                        </AccordionPanel>
                        <AccordionPanel label="Агент ">
                            <Box direction="column"
                                 width={"large"}
                                 pad="small"
                            >

                                <Text> Наименование пользователя</Text>
                                <TextInput
                                    className="form-input"
                                    placeholder="Наименование пользователя"
                                    value={value}
                                    width={"150px"}
                                    onChange={event => setValue(event.target.value)}
                                    style={{margin: "20px"}}
                                />
                                <Text> Пароль сервера</Text>
                                <TextInput
                                    className="form-input"
                                    placeholder="Пароль сервера"
                                    value={value}
                                    width={"150px"}
                                    onChange={event => setValue(event.target.value)}
                                    style={{margin: "20px"}}
                                />
                                <Text>Порт сервера</Text>
                                <TextInput
                                    className="form-input"
                                    placeholder="Порт  сервера"
                                    value={value}
                                    width={"150px"}
                                    onChange={event => setValue(event.target.value)}
                                    style={{margin: "20px"}}
                                />


                            </Box>
                        </AccordionPanel>
                    </Accordion>
                </Tab>
                <Tab title="Продвинутый метод">
                    <TextArea
                        placeholder="type here"
                        value={`name: quarkload
server-host: localhost
site-setup:
  schedules:
    - schedule:
        routing:
          - roadmap:
              url: "http://localhost"
              requestType: GET
              context: application/json
              statusCode: 200
          - roadmap:
        step-load:
          start: 1
          end: 10
          duration: 2min
          step: 1
  autostop:
    quantile: 90%
    responseLimit: 10ms
    time: 1h
  helpers:
    ssh-agent:
      host: {{ HASHICORP.SSH_HOST }}
      user: {{ HASHICORP.SSH_USER }}
      port: 22
      auth-method:
        user-auth:
          password: {{ HASHICORP.SSH_PASSWORD }}`}
                        onChange={event => setValue(event.target.value)}
                    />
                </Tab>
            </Tabs>


            <Box direction="row"
                 width={"large"}
                 pad="small">
                <Button style={{margin: "10px"}}
                        icon={<Edit/>}
                        label="Сохранить"
                        onClick={() => {
                        }}
                />
                <Button style={{margin: "10px"}}
                        icon={<Edit/>}
                        label="Проверить подключение"
                        onClick={() => {
                        }}
                />
                <Button style={{margin: "10px"}}
                        icon={<Edit/>}
                        label="Отменить"
                        onClick={() => {
                        }}
                />
            </Box>

        </div>
    )
}

export default AddTest
