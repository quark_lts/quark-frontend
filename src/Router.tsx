import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import {Accordion, AccordionPanel, Box, Grommet, Header, Heading, Menu, Text} from "grommet";
import Quark from "./components";
import HistoryPage from "./pages/HistoryPage";
import {hot} from "react-hot-loader/root";
import AddTest from "./pages/Test/AddTest";


const RoutingComponent = () =>
    <Grommet>
        <Router>
            <React.Fragment>
                <Header background="black">
                    <Text style={{padding: '10px'}}> QuarkLT</Text>
                    <Menu label="Управление" items={[{label: 'logout'}]}/>
                </Header>
                <Quark.Layout>
                    <Quark.Sidebar>
                        <Quark.Menu/>
                        <Accordion>
                            <AccordionPanel label="Test History">
                                <Box pad="medium" background="light-2">
                                    <Text>One</Text>
                                </Box>
                            </AccordionPanel>
                        </Accordion>
                    </Quark.Sidebar>
                    <Quark.Container>
                        <Switch>
                            <Route path="/new_test">
                                <AddTest/>
                            </Route>
                            <Route path="/history">
                                <HistoryPage/>
                            </Route>
                            <Route path="/nodes">

                            </Route>
                            <Route path="/settings">

                            </Route>
                        </Switch>
                    </Quark.Container>
                </Quark.Layout>
            </React.Fragment>
        </Router>
    </Grommet>;
export default hot(RoutingComponent);