import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import RoutingComponent from "./Router";

ReactDOM.render(<RoutingComponent />, document.getElementById('root'));

serviceWorker.unregister();
