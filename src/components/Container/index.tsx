import React from 'react'
import { Box } from 'grommet';
function Container({ children }: { children: any }) {
    return (
        <Box
            direction="column"
            border={{ color: 'brand', size: 'small' }}
            pad="medium"
            align="stretch"
            basis="full"
        >
            {children}

        </Box>
    )
}

export default Container
