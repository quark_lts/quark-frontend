import React from 'react'
import './index.css'
function Layout({ children }: { children: any }) {
    return (
        <div className="layout">
            {children}
        </div>
    )
}

export default Layout;
