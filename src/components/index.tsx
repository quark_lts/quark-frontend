import Sidebar from './Sidebar/index';
import Menu from './Menu/index';
import Container from './Container/index';
import Layout from './Layout/index';

export default {
    Sidebar,
    Menu,
    Container,
    Layout
}