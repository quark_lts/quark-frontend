import React from 'react'
import {Text} from 'grommet';
import {History, Test, Nodes, SettingsOption, ShieldSecurity} from 'grommet-icons';
import './index.css';
import {Link} from "react-router-dom";

const Item = ({label, icon, route}: { label: string, icon: JSX.Element, route: string }) =>
    (
        <Link to={route} style={{textDecoration: 'none'}}>
            <div className="menu__item">
                {icon}
                <Text className="menu__item__text">

                    {label}
                </Text>
            </div>
        </Link>);

const Menu = () => {

    return (
        <div className="quark__menu">
            <Item
                label={"New Test"}
                icon={<Test size="small"/>}
                route={"/new_test"}
            />
            <Item
                label={"Node"}
                route={"/nodes"}
                icon={<Nodes size="small"/>}
            />
            <Item
                label={"History"}
                route={"/history"}
                icon={<History size="small"/>}
            />
            <Item
                label={"Manage QuarkLT"}
                route={"/settings"}
                icon={<SettingsOption size="small"/>}
            />
            <Item
                label={"Credentials"}
                route={"/credentials"}
                icon={<ShieldSecurity
                    size="small"/>}
            />

        </div>
    )
}


export default Menu;