import React from 'react'
import './index.css';
function Sidebar({ children }: { children: JSX.Element[] }) {
    return (
        <div className="sidebar">
            {children}
        </div>
    )
}

export default Sidebar
