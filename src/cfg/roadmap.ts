interface Roadmap {
    url: string;
    requestType: string;
    context: string;
    statusCode: number;
}