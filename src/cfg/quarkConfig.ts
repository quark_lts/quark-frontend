import {SiteSetup} from "./siteSetup";

export interface QuarkConfig {
    name: string;
    'site-setup': SiteSetup;
    'server-host': string;
    helpers: any[];
}

