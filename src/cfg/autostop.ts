export interface Autostop {
    quantile: string;
    responseLimit: string;
    time: string;
}