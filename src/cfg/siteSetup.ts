import {Schedule} from "./schedule";
import {Autostop} from "./autostop";
import {Agent} from "./agent";

export interface SiteSetup {
    schedules: Schedule[];
    autoStop: Autostop;
    helpers: Agent[];
}