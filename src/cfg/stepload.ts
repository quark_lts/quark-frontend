interface Stepload {
    start: number;
    end: number;
    duration: number;
    step: number;
}