import {action} from "mobx";
import {QuarkConfig} from "./quarkConfig";
import {Schedule} from "./schedule";
import {Autostop} from "./autostop";

class Config {
    private config: QuarkConfig;

    constructor() {
        this.config = <QuarkConfig>{};
    }

    @action AddRoadmap = (url: string) => {
        this.config["site-setup"].schedules[0].routing.push({
                url,
                requestType: 'GET',
                context: 'application/json',
                statusCode: 200
            }
        );
    };

    @action AddSchedule = (schedule: Schedule) => {
        this.config["site-setup"].schedules.push(schedule);
    };

    @action AddAgent = (agentData: any) =>
        this.config.helpers.push(agentData);

    @action AddAutostop = (autostop: Autostop) => {
        this.config["site-setup"].autoStop = autostop;
    };

    get ConfigJson() {
        return this.config;
    }
}
