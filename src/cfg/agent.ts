export interface Agent {
    'ssh-agent': {
        host: string;
        user: string;
        port: number,
        'auth-method': {
            'user-auth': {
                password: string;
            }
        }
    }
}